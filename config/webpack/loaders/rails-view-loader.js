module.exports = {
  rules: [
    {
      test: /\.html\.(erb|slim|haml)$/,
      use: [
        "html-loader",
        {
          loader: "rails-view-loader"
        }
      ]
    }
  ]
};
// module.exports = {
//   test: /\.hbs$/,
//   loader: "handlebars-loader",
//   options: {
//     helperDirs: path.join(__dirname, "/../../../frontend/handlebars/helpers"),
//     precompileOptions: {
//       knownHelpersOnly: false
//     }
//   }
// };
