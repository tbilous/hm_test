const path = require("path");

module.exports = {
  test: /\.hbs$/,
  loader: "handlebars-loader",
  options: {
    helperDirs: path.join(__dirname, "/../../../frontend/handlebars/helpers"),
    precompileOptions: {
      knownHelpersOnly: false
    }
  }
};
