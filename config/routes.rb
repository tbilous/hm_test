Rails.application.routes.draw do

  resources :categories  do
    resources :posts, shallow: true do
      resources :comments, only: %i[create new], defaults: {context: 'posts', format: :json}
    end
    resources :comments, only: %i[create new], defaults: {context: 'categories', format: :json}
  end

  root 'categories#index'
end
