import categoriesCallback from "client/categories";
import createElementFromHTML from "client/client_utilites";

import "./ajax_categories.coffee";

const container = document.querySelector(".js-categories");

categoriesCallback(data => {
  if (window.location.pathname === `/categories/${data.id}`) {
    window.location = "/";
  }
  const content = container.querySelector(`#category_${data.id}`);

  if (content) {
    const newEl = createElementFromHTML(data.html);
    if (data.action === "update") {
      container.replaceChild(newEl, content);
    } else {
      container.removeChild(content);
    }
  } else {
    container.insertAdjacentHTML("afterbegin", data.html);
  }
});
