$('#posts').on 'ajax:success', '.js-post--edit--common', (event) ->
  [data, status, xhr] = event.detail
  html = event.detail[0]
  status = event.detail[1]
  xhr = event.detail[2]
  container = $(@).data('target')
  $(container).html(html)

$(document).on 'ajax:error', '.js-post-form--edit', (event) ->
  [data, status, xhr] = event.detail
  data = event.detail[0]
  App.utils.errorMessage(data)

$(document).on 'ajax:success', '#new_post', (event) ->
  [data, status, xhr] = event.detail
  data = event.detail[0]
  App.utils.successMessage(data.message)
  @.reset()

$(document).on 'ajax:error', '#new_post', (event) ->
  [data, status, xhr] = event.detail
  data = event.detail[0]
  App.utils.errorMessage(data)

$(document).on 'ajax:error', '#new_comment', (event) ->
  [data, status, xhr] = event.detail
  data = event.detail[0]
  App.utils.errorMessage(data)

$(document).on 'ajax:success', '#new_comment', (event) ->
  [data, status, xhr] = event.detail
  data = event.detail[0]
  App.utils.successMessage(data.message)
  @.reset()

