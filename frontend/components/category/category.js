import { categoryCallbacPost, categoryCallbacComment } from "client/category";
import createElementFromHTML from "client/client_utilites";

import "./ajax_category.coffee";

const containerPosts = document.querySelector(".js-posts");
const containerComments = document.querySelector(".js-comments");

categoryCallbacPost(data => {
  if (window.location.pathname === `/post/${data.id}`) {
    window.location = `/category/${data.category_id}`;
  }
  const content = containerPosts.querySelector(`#post_${data.id}`);
  if (content) {
    const newEl = createElementFromHTML(data.html);
    if (data.action === "update") {
      containerPosts.replaceChild(newEl, content);
    } else {
      containerPosts.removeChild(content);
    }
  } else {
    containerPosts.insertAdjacentHTML("afterbegin", data.html);
  }
});

categoryCallbacComment(data => {
  containerComments.insertAdjacentHTML("afterbegin", data.html);
});
