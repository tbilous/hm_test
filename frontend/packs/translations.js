/* eslint-disable object-shorthand */
import merge from "lodash/merge";
import { ru } from "../../config/locales/ru.yml";
import * as ruResponders from "../../config/locales/responders.ru.yml";

export default {
  ru: merge(ru, ruResponders.ru)
};
