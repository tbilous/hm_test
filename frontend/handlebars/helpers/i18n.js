const I18n = require("i18n-js");

module.exports = a => I18n.t(a);
