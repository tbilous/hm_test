module.exports = (a, b, opts) => {
  let c = {};
  if (a === b) {
    c = opts.fn(this);
  } else {
    c = opts.inverse(this);
  }
  return c;
};
