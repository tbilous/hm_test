/* eslint-disable no-console */
import createChannel from "client/cable";

let callback; // declaring a variable that will hold a function later
const container = document.getElementById("categories");

if (container) {
  createChannel(
    { channel: "CategoriesChannel" },
    {
      connected() {
        console.log("Connected");
      },
      received({ category }) {
        if (callback) callback.call(null, category.item);
      }
    }
  );
}
function categoriesCallback(fn) {
  callback = fn;
}

export default categoriesCallback;
