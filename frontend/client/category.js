/* eslint-disable no-console */
import createChannel from "client/cable";

const _ = require("lodash");

// let callback; // declaring a variable that will hold a function later

let post; // declaring a variable that will hold a function later
let comment; // declaring a variable that will hold a function later

const container = document.getElementById("category");

if (container) {
  createChannel(
    { channel: "CategoryChannel", category_id: container.dataset.category },
    {
      connected() {
        console.log("Connected");
      },
      received({ data }) {
        const key = _.first(Object.values(data));
        if (key === "posts") {
          if (post) post.call(null, data);
        } else if (key === "comments") {
          if (comment) comment.call(null, data);
        }
      }
    }
  );
}

// function categoryCallback(fn) {
//   callback = fn;
// }

function categoryCallbacPost(fn) {
  post = fn;
}

function categoryCallbacComment(fn) {
  comment = fn;
}

export { categoryCallbacComment, categoryCallbacPost };
