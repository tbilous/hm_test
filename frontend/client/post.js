/* eslint-disable no-console */
import createChannel from "client/cable";

let comment;

const container = document.getElementById("post");

if (container) {
  createChannel(
    { channel: "PostChannel", post_id: container.dataset.category },
    {
      connected() {
        console.log("Connected");
      },
      received({ data }) {
        comment.call(null, data);
      }
    }
  );
}

function postCallbacComment(fn) {
  comment = fn;
}

export default postCallbacComment;
