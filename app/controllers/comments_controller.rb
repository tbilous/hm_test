class CommentsController < ApplicationController
  include Contexted
  include Serialized
  before_action :set_context

  def create
    render_json @comment = @context.comments.create(strong_params)
  end

  private

  def strong_params
    params.require(:comment).permit(:author, :content)
  end

  def render_broadcast(item, data)
    job = "Broadcast#{@context.class.name.constantize}Job".constantize
    job.perform_later(slug: controller_name, id: item.id, html: data, category_id: @context.id)
  end
end
