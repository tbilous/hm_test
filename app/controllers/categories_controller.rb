class CategoriesController < ApplicationController
  include Serialized
  before_action :load_category, only: %i[show update destroy edit]

  respond_to :json, only: %i(edit)

  def index
    @category = Category.new
    @categories = Category.all.order(created_at: :desc)
  end

  def show
    # @category_comment = @category.comments.build
    @post = Post.new
    @posts = @category.posts.order(created_at: :desc)
    @comments = @category.comments
  end

  def create
    @category = Category.create(strong_params)
    render_json @category
  end

  def update
    @category.update(strong_params)
    render_json @category
  end

  def destroy
    render_json @category.destroy
  end

  def edit
    render json: helpers.c("#{controller_name}_form",
                           category: @category,
                           id: "edit_categoryr#{@category.id}", m: 'PATCH', tclass: 'js-category-form--edit')
  end

  private

  def strong_params
    params.require(:category).permit(:name, :description)
  end

  def load_category
    @category = Category.find(params[:id])
  end

  def render_broadcast(item, data)
    BroadcastCategoriesJob.perform_later item: { id: item.id, html: data, action: action_name }
  end
end
