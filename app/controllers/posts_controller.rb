class PostsController < ApplicationController
  include Serialized
  before_action :load_post, except: %i[create]

  def create
    @category = Category.find(params[:category_id])
    @post = @category.posts.create(strong_params)
    render_json @post
  end

  def update
    @post.update(strong_params)
    render_json @post
  end

  def destroy
    render_json @post.destroy
  end

  def show
    @comments = @post.comments.all
  end

  def edit
    render json: helpers.c("#{controller_name}_form", post: @post, id: "edit_post#{@post.id}",
                                                      m: 'PATCH', tclass: 'js-post-form--edit')
  end

  private

  def strong_params
    params.require(:post).permit(:name, :content, :file)
  end

  def load_post
    @post = Post.find(params[:id])
  end

  def render_broadcast(item, data)
    BroadcastCategoryJob.perform_later(slug: controller_name, id: item.id, category_id: item.category.id,
                                       html: data, action: action_name)
  end
end
