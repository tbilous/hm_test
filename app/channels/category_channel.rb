class CategoryChannel < ApplicationCable::Channel
  def subscribed
    reject if params['category_id'].blank?
    # binding.pry
    category = Category.find_by_id(params['category_id'])
    reject_subscription unless category.present?

    stream_from "category_#{params['category_id']}:posts"
    stream_from "category_#{params['category_id']}:comments"
  end
end
