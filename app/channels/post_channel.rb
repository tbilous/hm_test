class PostChannel < ApplicationCable::Channel
  def subscribed
    reject if params['post_id'].blank?
    post = Post.find_by_id(params['post_id'])
    reject unless post.present?

    stream_from "post_#{params['post_id']}:comments"
  end
end
