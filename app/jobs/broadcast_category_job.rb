class BroadcastCategoryJob < ApplicationJob
  queue_as :default

  def perform(item)
    ActionCable.server.broadcast "category_#{item[:category_id]}:#{item[:slug]}", data: item
  end
end
