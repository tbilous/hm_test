class BroadcastCategoriesJob < ApplicationJob
  queue_as :default

  def perform(item)
    ActionCable.server.broadcast 'categories', category: item
  end
end
