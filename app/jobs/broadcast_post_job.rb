class BroadcastPostJob < ApplicationJob
  queue_as :default

  def perform(item)
    ActionCable.server.broadcast "post_#{item[:category_id]}:#{item[:slug]}", data: item
  end
end
