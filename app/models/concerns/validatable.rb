module Validatable
  extend ActiveSupport::Concern

  private

  def validate_regex_name
    r = /\A(?=^(.*?\..*?)$)(?(1)\p{Lu}(\p{Any}+\.?{1})+\s+\p{Any}(\p{Any}+\.?{1})+(\s+(\p{Any}+\.?{1})+)*)\z/
    format = name =~ r
    errors.add(:name, :wrong_format_name, entry_name: name) if format.nil?
  end

  def validate_regex_author
    # rubocop:disable Metrics/LineLength
    r = /\A(?=^(.*?\..*?)$)(?(1)\p{Lu}(\p{Any}+\.?{1})+\s+\p{Lu}\p{Any}(\p{Any}+\.?{1})+(\s+(\p{Lu}\p{Any}+\.?{1})+)*)\z/
    # rubocop:enable Metrics/LineLength
    format = author =~ r
    errors.add(:author, :wrong_format_author, entry_name: author) if format.nil?
  end
end
