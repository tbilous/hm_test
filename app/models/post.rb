class Post < ApplicationRecord
  include Commentable
  include Validatable

  belongs_to :category
  has_attached_file :file

  validates_presence_of :name
  validate :validate_regex_name
  validates_attachment_size :file, less_than: 2.megabytes
  do_not_validate_attachment_file_type :file
end
