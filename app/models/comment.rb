class Comment < ApplicationRecord
  include Validatable
  belongs_to :commentable, polymorphic: true, touch: true

  validates_presence_of :author, :content
  validate :validate_regex_author
end
