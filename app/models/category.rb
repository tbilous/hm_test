class Category < ApplicationRecord
  include Commentable
  include Validatable

  has_many :posts, dependent: :destroy

  validates_presence_of :name
  validate :validate_regex_name

  # after_commit :broadcasted
  #
  # def broadcasted
  #   BroadcastCategoriesJob.perform_later self
  #   # BroadcastCategoryJob.perform_later self
  # end
end
