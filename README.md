# README

### Тестове завдання
* Опис: https://docs.google.com/document/d/1wtcfnoALkQgBq90I_0MLNhubzU2NxPM2cFM6qxZkvU8/edit
* Використовувались Ruby version 2.5.0, rails version 5.2
* Розгортання:
```
git clone git@bitbucket.org:tbilous/hm_test.git
cd hm_test
gem install rails -v '5.2'
bundle install
yarn install
mv config/database.yml.example config/database.yml
EDITOR="mate --wait" bin/rails credentials:edit
rake db:create
rake db:migrate
rake db:test:prepare
```
* Старт серверу `foreman start -f Procfile.dev`, адресса в браузері http://localhost:5000
* Тест `rspec spec/`
* Не реалізовано React + Redux + React-router 

### Примітка
* Для файлів використано гем paperclip - рішення спірне але формально не протирічить умовам завдання і швидке.
