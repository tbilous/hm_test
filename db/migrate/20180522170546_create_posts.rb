class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.references :category, foreign_key: true
      t.string :name, null: false, default: ''
      t.text :content, default: ''

      t.timestamps
    end
  end
end
