require 'rails_helper'

RSpec.describe CategoryChannel, type: :channel do
  let!(:category) { create(:category) }

  it 'subscribes to answer stream when id provided' do
    subscribe(category_id: category.id)
    expect(subscription).to be_confirmed
    expect(streams).to contain_exactly("category_#{category.id}:posts", "category_#{category.id}:comments")
  end

  it 'rejects when no id provided' do
    subscribe
    expect(subscription).to be_rejected
  end
end
