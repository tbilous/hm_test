require 'rails_helper'

RSpec.describe PostChannel, type: :channel do
  let(:category) { create(:category) }
  let!(:post) { create(:post, category_id: category.id) }

  it 'subscribes to answer stream when id provided' do
    subscribe(post_id: post.id)
    expect(subscription).to be_confirmed
    expect(streams).to contain_exactly("post_#{post.id}:comments")
  end

  it 'rejects when no id provided' do
    subscribe
    expect(subscription).to be_rejected
  end
end
