require 'rails_helper'

RSpec.describe CategoriesChannel, type: :channel do
  it 'subscribes to answer stream when id provided' do
    subscribe
    expect(subscription).to be_confirmed
    expect(streams).to contain_exactly('categories')
  end
end
