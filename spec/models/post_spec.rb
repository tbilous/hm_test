require 'rails_helper'
require_relative 'concerns/validatable'

RSpec.describe Post, type: :model do
  it { should belong_to(:category) }
  it { should have_many(:comments).dependent(:destroy) }
  it_behaves_like 'validatable'

  it { should validate_attachment_size(:file).less_than(2.megabytes) }
end
