require 'rails_helper'

shared_examples 'validatable' do
  describe 'validation :name_regexp' do
    array = ['category', 'Category', 'category category', 'Category c', 'C category', 'Category category']
    array.each do |i|
      context "Wrong validation #{i}" do
        let(:category) { build(:category, name: i) }
        it do
          category.valid?
          expect(category.errors.full_messages).not_to be_empty
        end
      end
    end

    context 'Success validation' do
      let(:category) { build(:category, name: 'Category category.') }
      it do
        category.valid?
        expect(category.errors.full_messages).to be_empty
      end
    end
  end
end
