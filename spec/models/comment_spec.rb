require 'rails_helper'

RSpec.describe Comment, type: :model do
  it { should belong_to :commentable }

  describe 'validation :name_regexp' do
    let(:category) { create(:category) }
    array = ['category', 'Category', 'category category', 'Category c', 'C category', 'Category category']
    array.each do |i|
      context "Wrong validation #{i}" do
        let(:comment) { build(:comment, author: i, commentable: category, commentable_type: 'Category') }
        it do
          comment.valid?
          expect(comment.errors.full_messages).not_to be_empty
        end
      end
    end

    context 'Success validation' do
      let(:commentable) { create(:post) }
      let(:comment) do
        build(:comment, author: 'Category Category.', commentable: category, commentable_type: 'Category')
      end
      it do
        comment.valid?
        expect(comment.errors.full_messages).to be_empty
      end
    end
  end
end
