require 'rails_helper'
require_relative 'concerns/validatable'

RSpec.describe Category, type: :model do
  it { should have_many(:posts).dependent(:destroy) }
  it { should have_many(:comments).dependent(:destroy) }
  it_behaves_like 'validatable'
end
