require 'rails_helper'
# require 'rack_session_access/capybara'
require 'puma'
require 'capybara/email/rspec'
require 'i18n'
require 'capybara/webkit'
require 'capybara/poltergeist'

RSpec.configure do |config|
  include ActionView::RecordIdentifier
  config.include AcceptanceHelper, type: :feature
  config.include FeatureMacros, type: :feature

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end

  Capybara.default_max_wait_time = 2
  Capybara.save_path = './tmp/capybara_output'
  Capybara.always_include_port = true # for correct app_host

  config.before(:each, type: :system) do
    driven_by :selenium_chrome_headless
  end

  # Capybara.javascript_driver = :webkit
  # Capybara.configure do |config|
  #   config.default_max_wait_time = 10 # seconds
  #   config.default_driver        = :selenium
  # end
  # Capybara.javascript_driver = :selenium_chrome
  Capybara.register_driver :poltergeist do |app|
  # noinspection RubyArgCount
  Capybara::Poltergeist::Driver.new(
    app,
    js_errors: false
  )
end
  Capybara.javascript_driver = :poltergeist
  # Capybara::Webkit.configure do |webkit|
  #   webkit.allow_url('10.0.2.15')
  #   webkit.allow_url('fonts.googleapis.com')
  #   webkit.allow_url('fonts.gstatic.com')
  #   webkit.allow_url('maxcdn.bootstrapcdn.com')
  # end
  #
  # config.before(:each, type: :feature) do
  #   # Note (Mike Coutermarsh): Make browser huge so that no content is hidden during tests
  #   Capybara.current_session.driver.browser.manage.window.resize_to(1_320, 1_500)
  # end

  Capybara.server = :puma

  config.use_transactional_fixtures = false

  config.before(:suite) { DatabaseCleaner.clean_with :truncation }

  config.before(:each) { DatabaseCleaner.strategy = :transaction }

  config.before(:each, js: true) { DatabaseCleaner.strategy = :truncation }

  config.before(:each) { DatabaseCleaner.start }

  config.append_after(:each) do
    DatabaseCleaner.clean
  end
end
