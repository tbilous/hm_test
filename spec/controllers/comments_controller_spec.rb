require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  let!(:category) { create(:category) }
  let!(:my_post) { create(:post, category: category) }

  shared_examples 'comments #create' do |context_name|
    context context_name do
      it { expect { subject }.to change(context.comments, :count).by(1) }

      it_behaves_like 'invalid params js', 'empty author', model: Comment do
        let(:form_params) { { author: '' } }
      end
      it_behaves_like 'invalid params js', 'empty content', model: Comment do
        let(:form_params) { { content: '' } }
      end
      it 'have broadcast' do
        perform_enqueued_jobs do
          expect { subject }
            .to have_broadcasted_to("#{context_name}_#{context_name.capitalize.constantize.first.id}:comments")
        end
      end
    end
  end

  describe 'POST #create' do
    let(:form_params) { {} }

    let(:params) do
      { comment: attributes_for(:comment).merge(form_params) }.merge(context_params)
    end

    subject { process :create, method: :post, params: params }

    it_behaves_like 'comments #create', 'category' do
      let(:context_params) { { category_id: category, context: 'category' } }
      let(:context) { category }
    end

    it_behaves_like 'comments #create', 'post' do
      let(:context_params) { { post_id: my_post, context: 'post' } }
      let(:context) { my_post }
    end
  end
end
