require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  let(:category) { create(:category) }

  describe 'GET #index' do
    let(:categories) { create_list(:category, 2) }
    before { get :index }

    it 'list all' do
      expect(assigns(:categories)).to match_array(categories)
    end

    it 'renders the index template' do
      expect(response).to render_template :index
    end

    it { expect(assigns(:category)).to be_a_new(Category) }
  end

  describe 'GET #show' do
    before { get :show, params: { id: category.id } }

    it 'render the show template' do
      expect(response).to render_template :show
    end
    it 'assign post ' do
      expect(assigns(:post)).to be_a_new(Post)
    end
  end

  describe 'POST #create' do
    let(:form_params) { {} }
    let(:params) do
      {
        category: attributes_for(:category).merge(form_params)
      }
    end

    subject { post :create, params: params, format: :json }

    it { expect { subject }.to change(Category, :count) }

    it_behaves_like 'invalid params concern', 'empty name', model: Category do
      let(:form_params) { { name: '' } }
    end
    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to('categories')
      end
    end
  end

  describe 'PATCH update' do
    let(:form_params) do
      {
        name: 'Ba bds.',
        description: 'z' * 6
      }
    end

    let(:params) do
      {
        id: category.id,
        format: :js,
        category: attributes_for(:category).merge(form_params)
      }
    end

    subject do
      patch :update, params: params, format: :json
      category.reload
    end

    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to('categories')
      end
    end

    describe do
      before { subject }
      it { expect(category.name).to eql params[:category][:name] }
      it { expect(category.description).to eql params[:category][:description] }

      it_behaves_like 'invalid params js', 'empty name', model: Category do
        let(:form_params) { { name: '' } }
        before { subject }
      end
      it_behaves_like 'invalid params js', 'empty description', model: Category do
        let(:form_params) { { description: '' } }
        before { subject }
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:category) { create(:category) }

    subject do
      delete :destroy, params: { id: category.id }
    end

    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to('categories')
      end
    end

    it { expect { subject }.to change { Category.count }.by(-1) }
  end

  describe 'GET #edit' do
    let(:category) { create(:category) }

    it 'returns http success' do
      get :edit, params: { id: category.id }
      expect(response).to have_http_status(200)
    end
  end
end
