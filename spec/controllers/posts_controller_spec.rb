require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let!(:category) { create(:category) }

  describe 'POST #create' do
    let(:form_params) { {} }

    let(:params) do
      { post: attributes_for(:post).merge(form_params), category_id: category.id }
    end

    subject { process :create, method: :post, params: params, format: :json }

    it { expect { subject }.to change(Post, :count).by(1) }

    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to("category_#{category.id}:posts")
      end
    end

    it_behaves_like 'invalid params js', 'empty name', model: Post do
      let(:form_params) { { name: '' } }
    end
  end

  describe 'PATCH #update' do
    let(:post) { create(:post, category: category) }
    new_param = 'Ddd ddd.'

    let(:params) do
      {
        post: { name: new_param },
        id: post.id,
        format: :json
      }
    end

    subject do
      process :update, method: :post, params: params
      post.reload
    end

    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to("category_#{post.category.id}:posts")
      end
    end

    describe do
      before { subject }
      it { expect(post.name).to eql new_param }
    end
  end

  describe 'DELETE #destroy' do
    let!(:post) { create(:post, category: category) }
    let(:params) do
      {
        id: post.id,
        format: :json
      }
    end

    it 'have broadcast' do
      perform_enqueued_jobs do
        expect { subject }.to have_broadcasted_to("category_#{post.category.id}:posts")
      end
    end

    subject { delete :destroy, params: params }

    it { expect { subject }.to change(Post, :count).by(-1) }
  end

  describe 'GET #show' do
    let(:post) { create(:post, category: category) }
    let(:params) { { id: post.id } }

    before { get :show, params: params }

    it 'render the show template' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #edit' do
    let(:post) { create(:post, category: category) }

    it 'returns http success' do
      get :edit, params: { id: post.id }
      expect(response).to have_http_status(200)
    end
  end
end
