# t.string "name", default: "", null: false
# t.text "content", default: "", null: false
FactoryBot.define do
  factory :post do
    name 'Post post.'
    content { Faker::FamilyGuy.quote }
    file do
      Rack::Test::UploadedFile
        .new("#{Rails.root}/spec/support/for_upload/noavatar.png", 'image/png')
    end
  end
end
