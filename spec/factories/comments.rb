# t.string "author", default: "", null: false
# t.text "content", default: "", null: false
FactoryBot.define do
  factory :comment do
    author 'Author Author.'
    content { Faker::FamilyGuy.quote }
  end
end
