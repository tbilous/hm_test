# t.string "name", default: "", null: false
# t.string "description", default: "", null: false
FactoryBot.define do
  factory :category do
    name 'Category category.'
    description { Faker::FamilyGuy.quote }
  end
end
