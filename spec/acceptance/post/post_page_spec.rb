require 'acceptance_helper'

feature 'Add catefory', %q{
  In order to create, update and destroy category
} do

  let(:category) { create(:category) }
  let!(:post) { create(:post, category_id: category.id) }
  let(:post_comment_attrib) { attributes_for(:comment) }

  context 'user can to create, update and destroy category', :js do
    it 'have websocket' do
      Capybara.using_session('author') do
        visit post_path(post.id)
      end

      Capybara.using_session('guest') do
        visit post_path(post.id)
      end

      Capybara.using_session('author') do
        within '#new_comment' do
          fill_in 'comment_author', with: post_comment_attrib[:author]
          fill_in 'comment_content', with: post_comment_attrib[:content]
          find('.btn').trigger('click')
        end

        perform_enqueued_jobs do
          within '.js-comments' do
            expect(page).to have_content post_comment_attrib[:author]
            expect(page).to have_content post_comment_attrib[:content]
          end
        end
      end

      Capybara.using_session('guest') do
        within '#comments' do
          expect(page).to have_content post_comment_attrib[:author]
          expect(page).to have_content post_comment_attrib[:content]
        end
      end
    end
  end
end
