require 'acceptance_helper'

feature 'Add catefory', %q{
  In order to create, update and destroy category
} do

  let!(:category) { create(:category) }
  let(:post_attrib) { attributes_for(:post) }
  let(:category_comment_attrib) { attributes_for(:comment) }

  context 'user can to create, update and destroy category', :js do
    it 'have websocket' do
      Capybara.using_session('author') do
        visit category_path(category.id)
      end

      Capybara.using_session('guest') do
        visit category_path(category.id)
      end

      Capybara.using_session('author') do
        within '#new_comment' do
          fill_in 'comment_author', with: category_comment_attrib[:author]
          fill_in 'comment_content', with: category_comment_attrib[:content]
          find('.btn').trigger('click')
        end

        perform_enqueued_jobs do
          within '.js-comments' do
            expect(page).to have_content category_comment_attrib[:author]
            expect(page).to have_content category_comment_attrib[:content]
          end
        end
      end

      Capybara.using_session('guest') do
        within '#comments' do
          expect(page).to have_content category_comment_attrib[:author]
          expect(page).to have_content category_comment_attrib[:content]
        end
      end

      Capybara.using_session('author') do
        within '#new_post' do
          fill_in 'post_name', with: post_attrib[:name]
          fill_in 'post_content', with: post_attrib[:content]
          find('.btn').trigger('click')
        end

        perform_enqueued_jobs do
          within '.js-posts' do
            expect(page).to have_content post_attrib[:name]
            expect(page).to have_content post_attrib[:content]
          end
        end
      end

      Capybara.using_session('guest') do
        within '.js-posts' do
          expect(page).to have_content post_attrib[:name]
          expect(page).to have_content post_attrib[:content]
        end
      end

      Capybara.using_session('author') do
        within '.js-posts--item' do
          click_on t('edit')
        end
        within '.js-post-form--edit' do
          fill_in 'post_name', with: post_attrib[:name]
          fill_in 'post_content', with: 'New desc'
          find('.btn').trigger('click')
        end
        perform_enqueued_jobs do
          within '.js-posts' do
            expect(page).to have_content post_attrib[:name]
            expect(page).to_not have_content post_attrib[:content]
            expect(page).to have_content 'New desc'
          end
        end
      end

      Capybara.using_session('guest') do
        within '.js-posts' do
          expect(page).to have_content post_attrib[:name]
          expect(page).to_not have_content post_attrib[:content]
          expect(page).to have_content 'New desc'
        end
      end

      Capybara.using_session('author') do
        within '.js-posts--item' do
          click_on t('delete')
        end

        perform_enqueued_jobs do
          within '.js-posts' do
            expect(page).to_not have_content post_attrib[:name]
            expect(page).to_not have_content 'New desc'
          end
        end
      end
    end
  end
end
