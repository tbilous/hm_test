require 'acceptance_helper'

feature 'Add catefory', %q{
  In order to create, update and destroy category
} do

  let(:category_attrib) { attributes_for(:category) }

  context 'user can to create, update and destroy category', :js do
    it 'have websocket' do
      Capybara.using_session('author') do
        visit root_path
      end

      Capybara.using_session('guest') do
        visit root_path
      end

      Capybara.using_session('author') do
        within '#new_category' do
          fill_in 'category_name', with: category_attrib[:name]
          fill_in 'category_description', with: category_attrib[:description]
          find('.btn').trigger('click')
        end
        perform_enqueued_jobs do
          within '.js-categories' do
            expect(page).to have_content category_attrib[:name]
            expect(page).to have_content category_attrib[:description]
          end
        end
      end

      Capybara.using_session('guest') do
        within '.js-categories' do
          expect(page).to have_content category_attrib[:name]
          expect(page).to have_content category_attrib[:description]
        end
      end

      Capybara.using_session('author') do
        within '.js-categories--item' do
          click_on t('edit')
        end
        within '.js-category-form--edit' do
          fill_in 'category_name', with: category_attrib[:name]
          fill_in 'category_description', with: 'New desc'
          find('.btn').trigger('click')
        end
        perform_enqueued_jobs do
          within '.js-categories' do
            expect(page).to have_content category_attrib[:name]
            expect(page).to_not have_content category_attrib[:description]
            expect(page).to have_content 'New desc'
          end
        end
      end

      Capybara.using_session('guest') do
        within '.js-categories' do
          expect(page).to have_content category_attrib[:name]
          expect(page).to_not have_content category_attrib[:description]
          expect(page).to have_content 'New desc'
        end
      end

      Capybara.using_session('author') do
        within '.js-categories--item' do
          click_on t('delete')
        end

        perform_enqueued_jobs do
          within '.js-categories' do
            expect(page).to_not have_content category_attrib[:name]
            expect(page).to_not have_content 'New desc'
          end
        end
      end
    end
  end
end
