require 'rails_helper'
require 'acceptance_helper'
feature 'Attributes of pages', %q{
  All pages have title
} do

  let(:category) { create(:category) }
  let(:post) { create(:post, category_id: category.id) }
  context 'visit on pages', js: true do
    scenario 'Categories page have title' do
      visit categories_path
      expect(page).to have_title "Hm Test #{t('categories.index.title')}"
    end
    scenario 'Category page have title' do
      visit category_path(category.id)
      expect(page).to have_title t('categories.show.title', entry_name: category.name)
    end
    scenario 'Post page have title' do
      visit post_path(post.id)
      expect(page).to have_title t('posts.show.title', entry_name: post.name)
    end
  end
end
